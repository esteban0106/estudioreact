import { eventos } from "../data/eventos"


export const getProgById = ( id = '') => {
    return eventos.find(event => event.id === id);
}