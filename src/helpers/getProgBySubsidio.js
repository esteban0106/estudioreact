import { eventos } from '../data/eventos';

export const getProgBySubsidio = ( subsidio ) => {

    const validSubsidio = [ 'aliado estrategico', 'aliado comercial'];

    if (!validSubsidio.includes(subsidio)){
        console.log(`El subsidio ${subsidio} no es permitido`);
    }


    return eventos.filter( sub => sub.subsidio === subsidio);
}