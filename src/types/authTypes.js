

export const authTypes = {
    login: '[login] Login',
    logout: '[logout] Logout'
}