import { LoginScreen } from "../components/login/LoginScreen"

export const AuthRouter = () => {
    return (

        <div className="auth__main">

            <aside className="auth__image">
            </aside>
            <div className="auth__box-container">
            <LoginScreen />
                <div className="auth__box-container-decoration">
                    
                </div>

            </div>
        </div>
    )
}
