import React from 'react'
import { Routes, Route, Navigate } from "react-router-dom";
// import { makeStyles/* , Backdrop, CircularProgress */  } from '@mui/styles';
import { AuthRouter } from './AuthRouter';
import { DashboardRouter } from './DashboardRouter';
import { PrivateRoute } from './PrivateRoute';
import { PublicRoute } from './PublicRoute';


export const AppRouter = () => {

  return (
    <div>
        <Routes>

          <Route path="*" element={<Navigate to='/login' />} />
          <Route path="/login" element={
            <PublicRoute >
              <AuthRouter />
            </PublicRoute>
          }
          />
          <Route path="/dashboard/*" element={
            <PrivateRoute>
              <DashboardRouter />
            </PrivateRoute>
          }
          />
        </Routes>
    </div>
  )
}