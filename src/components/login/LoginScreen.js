// import React, { useContext } from 'react'
// import { useNavigate } from 'react-router-dom'
// import { AuthContext } from '../../auth/authContext';
// import { authTypes } from '../../types/authTypes';

// export const LoginScreen = () => {

//   const navigate = useNavigate();
//   const {dispatch} = useContext(AuthContext);

//   const handleLogin = () => {

//     const action = {
//       type : authTypes.login,
//       payload: {
//         name: 'Teban',
//         lastname: 'rojas'
//       }
//     }

//     dispatch(action)

//     const lastPath = localStorage.getItem('lastPath') || '/dashboard/subsidio';

//     navigate(lastPath, { replace: true });
//   }

//   return (
//     <div className="container mt-5">

//         <h1>Login</h1>
//         <hr />

//         <button className="btn btn-primary"
//                 onClick={handleLogin}>
//           Login
//         </button>
//     </div>
//   )
// }

import { useNavigate } from 'react-router-dom'
import { Grid, InputAdornment } from '@mui/material';
import React, { useContext, useState } from 'react'
import { useForm } from '../../hooks/useForm';
import { CssTextField } from '../ui/InputOutlined';
import LockIcon from '@mui/icons-material/Lock';
import AlternateEmailIcon from '@mui/icons-material/AlternateEmail';
import VisibilityIcon from '@mui/icons-material/Visibility';
import VisibilityOffIcon from '@mui/icons-material/VisibilityOff';
import { AuthContext } from '../../auth/authContext';
import { authTypes } from '../../types/authTypes';



export const LoginScreen = ({ history }) => {

  const navigate = useNavigate();
  const { dispatch } = useContext(AuthContext);

  const handleLogin = () => {
    const action = {
      type: authTypes.login,
      payload: {
        name: 'Teban',
        lastname: 'rojas'
      }
    }

    dispatch(action)

    const lastPath = localStorage.getItem('lastPath') || '/dashboard/subsidio';

    navigate(lastPath, { replace: true });
  }

  const [showPassword, setShowPassword] = useState(false)
  // const [valid, setValidator] = useState({
  //     email: '',
  //     password: ''
  // })

  const [formValues, handleInputChange] = useForm({
    email: "",
    password: ""

  })

  const { email, password } = formValues;


  const handleShowPassword = () => {
    if (!showPassword) {
      setShowPassword(true)
    } else {
      setShowPassword(false)

    }
  }
  /*
      const handleValidator = (e, type) => {
          console.log(e)
          handleInputChange(e);
          console.log(email)
          const value = e.target.value;
          switch (type) {
              case "password":
                  if (value.length < 6) {
                      setValidator({
                          ...valid,
                          password: '* La contraseña debe tener 6 caracteres mínimo'
                      })
                  } else {
                      setValidator({
                          ...valid,
                          password: null
                      })
                  }
                  break;
              case "email":
                  if (value.length < 3) {
                      setValidator({
                          ...valid,
                          email: '* El Usuario no es valido'
                      })
  
                  } else {
                      setValidator({
                          ...valid,
                          email: null
                      })
                  }
                  break;
              default:
                  if (value.length === 0)
                      setValidator({
                          ...valid,
                          [e.target.name]: '* Campo obligatorio'
  
                      })
                  break;
          }
      }
  
  
      const isFormValid = () => {
  
  
          if (!valid.email && !valid.password && email && password) {
              return true
          } else {
              return false
          }
  
      }
  
   */


  return (
    <div className="auth__login-box">
      <div className="auth__space"></div>
      <span className="auth__title-login">Iniciar sesión</span><br />
      <p>Digita tu usuario y contraseña para ingresar  al aplicativo</p>
      <br />
      <form key="formLogin" autoComplete="off">
        <input autoComplete="nope" name="hidden" type="text" style={{ display: "none" }} />

        <Grid container direction={"column"} spacing={1}>
          <Grid item>
            <span className='auth__label'>Usuario</span><br />
            <CssTextField
              key="email123"
              id="email123"
              name="email"
              value={email ? email : ""}
              fullWidth
              // onChange={(e) => handleValidator(e, 'email')}
              placeholder="example_username"
              type="text"
              label=""
              variant='outlined'
              // error={!!valid.email}
              // helperText={(valid.email ? valid.email : '')}

              size="small"
              autoComplete='off'
              InputProps={{
                style: { fontFamily: 'Source Sans Pro' },
                startAdornment: (
                  <InputAdornment position="start"> {< AlternateEmailIcon />
                  } </InputAdornment>
                )
              }}
            />
            <br />
          </Grid>
          <Grid item>

            <br />
            <span className='auth__label'>Contraseña</span><br />

            <CssTextField
              key="password123"
              id="password123"
              name="password"
              value={password ? password : ""}
              // onChange={(e) => handleValidator(e, 'password')}
              fullWidth
              placeholder="*******"
              m={5}

              type={showPassword ? "text" : "password"}
              label=""
              variant='outlined'
              // error={!!valid.password}
              // helperText={(valid.password ? valid.password : '')}
              size="small"
              autoComplete='off'

              InputProps={{
                style: { fontFamily: 'Source Sans Pro' },
                endAdornment: (
                  <InputAdornment position="end" onClick={handleShowPassword} className="pointer"> {
                    showPassword ?
                      < VisibilityIcon />
                      :
                      < VisibilityOffIcon />
                  } </InputAdornment>
                ),
                startAdornment: (
                  <InputAdornment position="start"> {
                    < LockIcon />
                  } </InputAdornment>
                )
              }}
            />
            <br />
          </Grid>
          <Grid item>

            <br />

            <button className="auth__button" /* type="submit" disabled={!isFormValid()} */ onClick={handleLogin} >
              Ingresar
            </button><br />
          </Grid>
        </Grid>

      </form>
    </div>
  )
}
