import React from 'react';
import { ProgramasList } from '../programas/ProgramasList';

export const AliadosScreen = () => {
  return (
    <div>
        <h1>Aliados</h1>
        <ProgramasList subsidio={'aliado estrategico'}/>
    </div>
  )
}
