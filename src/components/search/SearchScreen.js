import React, { useMemo } from 'react'
import { useNavigate, useLocation } from 'react-router-dom'
import queryString from 'query-string'

import {useForm} from '../../hooks/useForm'
import { getProgByName } from '../../helpers/getProgByName'
import { ProgramasCard } from '../programas/ProgramasCard';

export const SearchScreen = () => {

  const navigate = useNavigate();
  const location = useLocation();
  const { q = '' }= queryString.parse(location.search);

  const [ formValue, handleInputChange] = useForm({
    searchText: q,
  });

  const {searchText} = formValue;

  const eventoFiltrado = useMemo(() => getProgByName(q),[q]);
  
  const handleSearch = (e) => {
    e.preventDefault(); 
    navigate(`?q=${searchText}`);
    

  }
  

  return (
    <>
      <h1>Busquedas</h1>
      <hr/>
      <div className='row'>
        <div className='col-5'>
          <h4>Buscar evento de un programa especial</h4>
          <hr />

          <form onSubmit={(e) => handleSearch(e)}>
            <input
              type="text"
              placeholder="Buscar eventos"
              className="form-control"
              name="searchText"
              autoComplete="off"
              value={searchText}
              onChange={handleInputChange}
            />

            <button 
              className='btn btn-outline-primary mt-5'
              type="submit"
              >
                Buscar ...
              </button>

          </form>
        </div>    
        <div className='col-7'>
          <h4>Resultados</h4>
          <hr/>

          {
            (q === '')
              ? <div className='alert alert-info'> Ingresa una busqueda</div>
              : (eventoFiltrado.length === 0) && <div className='alert alert-danger'>
                No hay resultados
              </div>
          }

          {
            eventoFiltrado.map( event => (
              <ProgramasCard 
                key={event.id}
                {...event}
              />
            ))
          }
        </div>    
      </div>
    </>
  )
}
