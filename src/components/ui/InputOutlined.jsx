import { lighten, TextField } from "@mui/material";
import { withStyles } from '@mui/styles';

   export const CssTextField = withStyles({

        root: {
            textOverflow:"ellipsis",
            fontFamily: 'Source Sans Pro',
            margin: '0px',
            color: '#3c3c3c',
            '&:hover .MuiOutlinedInput-root  .MuiOutlinedInput-notchedOutline': {
                borderColor: `#1C78E6`,

            },
            "&:hover .MuiInputLabel-outlined": {
                color: `#1C78E6`,
            },

            "& .MuiInputLabel-outlined.Mui-focused": {
                color: lighten(`#1C78E6`, 0.2)
            },
            "& .MuiOutlinedInput-root.Mui-focused .MuiOutlinedInput-notchedOutline": {
                borderColor: lighten(`#1C78E6`, 0.2)
            },

            '& > span': {
                      maxWidth: '100%',
                      overflow: 'hidden',
                      textOverflow: 'ellipsis',
                         paddingLeft: 5,
                         paddingRight: 5,
                     display: 'block',
       },

        },

    })(TextField);
  