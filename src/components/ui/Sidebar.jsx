import LogoutIcon from '@mui/icons-material/Logout';
import AssessmentIcon from '@mui/icons-material/Assessment';
import EventNoteIcon from '@mui/icons-material/EventNote';
import { NavLink, useNavigate } from 'react-router-dom';
import { AccountCogOutline } from '../../assets/icons/AccountCogOutline';
import { Vector } from '../../assets/icons/Vector';
import { AccountGroupOutline } from '../../assets/icons/AccountGroupOutline';
import HistoryIcon from '@mui/icons-material/History';
import { CajasanWhite } from '../../assets/icons/CajasanWhite';
import avatar from '../../assets/profile.svg'
import { authTypes } from '../../types/authTypes';
import { useContext } from 'react';
import { AuthContext } from '../../auth/authContext';

export const Sidebar = () => {

    const user = JSON.parse(localStorage.getItem('user'));
    const {dispatch} = useContext(AuthContext);

    const navigate = useNavigate();

    const handleLogout = () => {
        const action = {
            type : authTypes.logout
          }
      
          dispatch(action)
        navigate("/login", { replace: true });
    }



    return (

        <div className="home__sidebar"  >
            <div className="home__sidebar-logo pointer" >
                < CajasanWhite />
            </div>
            <div className="home__sidebar-navbar">
                <div className="home__sidebar-avatar-border">
                    <img
                        alt="Avatar"
                        className="home__sidebar-avatar mt-1"
                        src={avatar}
                    >
                    </img>
                </div>
                <h3 className="mt-1">{user.name}</h3>
                <p> Rol: {user.lastname}</p>
            </div>
            <div className="home__sidebar-navbar-items mt-1">

                {<NavLink
                    className={({ isActive }) => 'home__sidebar-item' + (isActive ? '-active' : '')}
                    to="/reportes">
                    <div className="home__row">

                        <div className="home__sidebar-icon">
                    < AssessmentIcon />
                        </div>
                        <div className='home__p'> Reportes</div>
                    </div>
                </NavLink>}
                {<NavLink 
                
                    className={({ isActive }) => 'home__sidebar-item' + (isActive ? '-active' : '')}
                    to="/aliados">
                    <div className="home__row">
                        <div className="home__sidebar-icon">
                            < EventNoteIcon />
                        </div>
                        <div className='home__p'> Programas Eventos</div>
                    </div>

                </NavLink> 
                }

                {<NavLink 
                
                    className={({ isActive }) => 'home__sidebar-item' + (isActive ? '-active' : '')}
                    to="/consultados">
                    <div className="home__row">
                        <div className="home__sidebar-icon">
                            < HistoryIcon />
                        </div>
                        <div className='home__p'> Servicios</div>
                    </div>

                </NavLink>}
                <NavLink 
                    className={({ isActive }) => 'home__sidebar-item' + (isActive ? '-active' : '')}
                    to="/afiliados">
                    <div className="home__row">

                        <div className="home__sidebar-icon">
                            < AccountGroupOutline />
                        </div>
                        <div className='home__p'> Presupuestos</div>
                    </div>
                </NavLink>


                {<NavLink 
                
                className={({ isActive }) => 'home__sidebar-item' + (isActive ? '-active' : '')}
                    to="/usuarios">
                    <div className="home__row">
                        <div className="home__sidebar-icon">
                            < AccountCogOutline />
                        </div>
                        <div className='home__p'> Liquidaciones</div>
                    </div>

                </NavLink>}

                {<NavLink 
                
                className={({ isActive }) => 'home__sidebar-item' + (isActive ? '-active' : '')}
                    to="/usuarios">
                    <div className="home__row">
                        <div className="home__sidebar-icon">
                            < AccountCogOutline />
                        </div>
                        <div className='home__p'> Programas</div>
                    </div>

                </NavLink>}

                <hr className="mt-5 mb-1 ml-5" />
                <div className="home__sidebar-item pointer"
                >
                    <div className="home__row" onClick={handleLogout}>

                        <LogoutIcon />
                        <div className='home__p'> Cerrar sesión</div>
                    </div>
                </div>


            </div>
        </div >
    )
}
