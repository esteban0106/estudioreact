import { Link } from 'react-router-dom';

const  eventoImage = require.context('../../assets', true);

export const ProgramasCard = ({id,
    nombre,
    subsidio,
    empresa,
    sector,
    ciudad}) => {
  return (
    <div className='col'>
        <div className='card'>
            <div className='col-4'>
                <img src={eventoImage(`./${id}.jpg`)} className="card-img" alt={id}/>
                
            </div>
            <div className='col-8'>
                <div className='card-body'>
                    <h5 className='card-title'>{nombre}</h5>
                    <p className='card-text'>{empresa}</p>
                    <p className='card-text'>
                        <small className='text-muted'>{sector}</small>
                    </p>
                    <Link to={`/dashboard/eventos/${id}`}>
                        evento mass...
                    </Link>
                </div>
            </div>
        </div>
    </div>
  )
}
