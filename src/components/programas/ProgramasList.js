import React, { useMemo } from 'react'
import { getProgBySubsidio } from '../../helpers/getProgBySubsidio'
import { ProgramasCard } from './ProgramasCard';

export const ProgramasList = ({subsidio}) => {

    const eventos = useMemo( () => getProgBySubsidio( subsidio ), [subsidio]);

  return (
    <>
        <h3>Lista de programas - {subsidio} </h3>
        <div className='row rows-cols-1 row-cols-md-3 g-3'>
            {
                eventos.map( evento => (
                    <ProgramasCard key={evento.id}
                    {...evento}/>
                ))
            }
        </div>

    </>
  )
}
