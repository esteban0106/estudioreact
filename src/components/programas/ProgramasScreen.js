import React, { useMemo } from 'react'
import { Navigate, useNavigate, useParams} from 'react-router-dom'
import { getProgById } from '../../helpers/getProgById';

const  eventoImage = require.context('../../assets', true);

export const ProgramasScreen = () => {

  const {eventoId} = useParams();
  const navigate = useNavigate();

  const event = useMemo ( () => getProgById(eventoId),[eventoId]);

  const handleBack = () => {
    navigate(-1);
  }

  if (!event ){
    return <Navigate to='/dashboard/subsidio'/>
  }

  return (
    <div className='col'>
        <div className='card'>
            <div className='col-4'>
                <img src={eventoImage(`./${eventoId}.jpg`)} className="card-img" alt={eventoId}/>
                
            </div>
                <div className='card-body'>
                    <h5 className='card-title'>{event.nombre}</h5>
                    <p className='card-text'>{event.empresa}</p>
                    <p className='card-text'>
                        <small className='text-muted'>{event.sector}</small>
                    </p>
                </div>
                <button className='btn btn-outline-info' onClick={handleBack}>
                  Regresar
                </button>
        </div>
    </div>
  )
}
