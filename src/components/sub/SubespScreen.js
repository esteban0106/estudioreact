import React from 'react'
import { ProgramasList } from '../programas/ProgramasList'

export const SubespScreen = () => {
  return (
    <div>
        <h1>Subsidio en especie</h1>
        <ProgramasList subsidio={'aliado comercial'}/>
    </div>
  )
}
