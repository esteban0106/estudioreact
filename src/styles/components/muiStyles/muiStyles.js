import { makeStyles } from '@mui/styles';

export const useStyles = makeStyles({
   
root:{
    fontFamily: ['Source Sans Pro', 'Open Sans'],
    margin: '0px',
    color: '#3c3c3c',
    width:'80%',

    '&:hover':{
        borderColor:'lighten("#1C78E6",20) !important'
    }
},




});

