export const eventos = [
    {
        'id': 'al_1',
        'nombre': 'Argos',
        'subsidio': 'aliado estrategico',
        'empresa':'Cementeria',
        'sector':'Industria',
        'ciudad':'Floridablanca'
    },
    {
        'id': 'al_2',
        'nombre': 'Ecopetrol',
        'subsidio': 'aliado estrategico',
        'empresa':'Petrolera',
        'sector':'Hidrocarburos',
        'ciudad':'Barrancabermeja'
    },
    {
        'id': 'al_3',
        'nombre': 'ETB',
        'subsidio': 'aliado estrategico',
        'empresa':'Telefonía',
        'sector':'Telecomunicaciones',
        'ciudad':'Bogotá'
    },
    {
        'id': 'al_4',
        'nombre': 'Noel',
        'subsidio': 'aliado estrategico',
        'empresa':'Alimentos',
        'sector':'Industria',
        'ciudad':'Medellín'
    },
    {
        'id': 'al_5',
        'nombre': 'Energías',
        'subsidio': 'aliado estrategico',
        'empresa':'Energías',
        'sector':'Industria',
        'ciudad':'Pereira'
    },
    {
        'id': 'al_6',
        'nombre': 'Banco Mundial',
        'subsidio': 'aliado estrategico',
        'empresa':'Banca',
        'sector':'Bancario',
        'ciudad':'Bogotá'        
    },
    {
        'id': 'se_aliado1',
        'nombre': 'Praxis',
        'subsidio': 'aliado estrategico',
        'empresa':'Educación', 
        'sector':'Educación',
        'ciudad':'Bucaramanga'
    },
    {
        'id': 'se_aliado2',
        'nombre': 'Villa Gabby',
        'subsidio': 'aliado estrategico',
        'empresa':'Turismo', 
        'sector':'Turismo', 
        'ciudad':'Bucaramanga',
    
        
    },
    {
        'id': 'se_aliado3',
        'nombre': 'Bodytech',
        'subsidio': 'aliado estrategico',
        'empresa':'Centro médico',
        'sector':'Deporte',
        'ciudad':'Floridablanca'
    },
    {
        'id': 'se_aliado4',
        'nombre': 'Aldia',
        'subsidio': 'aliado estrategico',
        'empresa':'Ferreteria',
        'sector':'Ferretería',
        'ciudad':'Medellín'
    },
    {
        'id': 'se_aliado5',
        'nombre': 'Ahorra mas',
        'subsidio': 'aliado estrategico',
        'empresa':'Banca',
        'sector':'Bancario',
        'ciudad':'Bucaramanga'
    },
    {
        'id': 'se_aliado6',
        'nombre': 'Universidad Pontificia',
        'subsidio': 'aliado estrategico',
        'empresa':'Educación',
        'sector':'Educación',
        'ciudad':'Bucaramanga'
    },
    // {
    //     'id': 'se_cajasan',
    //     'nombre': 'Cajasan supermercados',
    //     'subsidio': 'aliado comercial',
    //     'empresa':'Canasta Familiar', 
    //     'sector':'Canasta Familiar', 
    //     'ciudad':'Bucaramanga'
    // },
    {
        'id': 'se_escolar',
        'nombre': 'Feria escolar Cajasan',
        'subsidio': 'aliado comercial',
        'empresa':'Cajasan',
        'sector':'Supermercado',
        'ciudad':'Bucaramanga'
    },
    {
        'id': 'se_lapiz',
        'nombre': 'Feria escolar SanGil',
        'subsidio': 'aliado comercial',
        'empresa':'Cajasan Sangil',
        'sector':'Supermercado', 
        'ciudad':'San Gil'
    },
    {
        'id': 'se_turismo',
        'nombre': 'Turismo Santander',
        'subsidio': 'aliado comercial',
        'empresa':'Cajasan',
        'sector':'Recreación',
        'ciudad':'Panachi'        
    },
    // {
    //     'id': 'se_vacuma',
    //     'nombre': 'Covid-19',
    //     'subsidio': 'aliado comercial',
    //     'empresa':'Cajasan IPS',
    //     'sector':'Salud', 
    //     'ciudad':' Piedecuesta'
    // }
]