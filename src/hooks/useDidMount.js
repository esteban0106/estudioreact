import { useEffect, useState } from "react"


// Hook que nos dice si un componente ya fue renderizado por primera vez

export const useDidMount = () => {
    const [didMount, setDidMount] = useState(false)
    useEffect(() => { setDidMount(true) }, [])

    return didMount
}